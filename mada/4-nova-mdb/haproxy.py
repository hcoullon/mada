from mad import *
import time


class HAProxy(Component):

    def create(self):
        self.places = [
            'initiated',
            'deployed'
        ]

        self.transitions = {
            'deploy': ('initiated', 'deployed', self.deploy)
        }

        self.dependencies = {
            'facts': (DepType.USE, ['deploy']),
            'mdbd': (DepType.DATA_USE, ['deploy']),
            'kstd': (DepType.DATA_USE, ['deploy']),
            'novad': (DepType.DATA_USE, ['deploy']),
            'glad': (DepType.DATA_USE, ['deploy']),
            'neud': (DepType.DATA_USE, ['deploy']),
            'haproxy': (DepType.PROVIDE, ['deployed'])
        }

    def deploy(self):
        time.sleep(1)
