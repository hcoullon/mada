# Content

This directory contains one sub-directory for each version of the OpenStack commissioning described in the submission.
For each one one `python` file is defined per component and an additional file `openstack.py` contains the Madeus Assembly as well as the MADA code.