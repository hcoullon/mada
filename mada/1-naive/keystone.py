from mad import *
import time

class Keystone(Component):

    def create(self):
        self.places = [
            'initiated',
            'pulled',
            'deployed'
        ]

        self.transitions = {
            'pull': ('initiated', 'pulled', self.pull),
            'deploy': ('pulled', 'deployed', self.deploy)
        }

        self.dependencies = {
            'mariadb': (DepType.USE, ['deploy']),
            'mdbd': (DepType.DATA_USE, ['deploy']),
            'kstd': (DepType.DATA_PROVIDE, ['initiated']),
            'keystone': (DepType.PROVIDE, ['deployed'])
        }

    def pull(self):
        time.sleep(1)

    def deploy(self):
        time.sleep(1)
