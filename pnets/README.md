# Content

This directory contains one sub-directory for each version of the OpenStack commissioning described in the submission.

For each one a `.cts` file is given.
This is a [Roméo](http://romeo.rts-software.org/) format, Roméo being the model checker used within the experiments of the submission.

The `.cts` file has been generated as an intermediate file by MADA. It represents the time Petri net associated to the Madeus software commissioning, as well as the properties to check.

One can note that in `4-nova-mdb` an additional `.cts` file is given. It represents the experiment with errors on time intervals in the submission.