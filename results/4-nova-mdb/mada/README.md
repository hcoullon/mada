# Content

- `openstack4_nova_mdb_results.org`: traces obtained from Roméo for each experiment
- `openstack4_nova_mdb_gantt.py`: python file to generate the Gantt diagram in gnuplot
- `openstack4_nova_mdb_gantt.gnu`: gnuplot file of the Gantt diagram
- `openstack4_nova_mdb_gantt.pdf`: pdf file of the Gantt diagram