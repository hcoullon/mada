import sys
sys.path.insert(0, '../../')
from gnuplot_gantt import *

tasks = [
    ("glance deploy", 180,271),
    ("neutron deploy",177,377),
    ("nova deploy",170,359),
    ("nova register",156,207),
    ("glance register",156,180),
    ("neutron register",156,177),
    ("nova upg-db",71,170),
    ("nova upg-api-db",71,115),
    ("kst deploy",58,156),
    ("neutron config",58,84),
    ("nova create-db",58,71),
    ("glance config",58,66),
    ("mariadb check",45,58),
    ("mariadb register",45,49),
    ("mariadb restart",31,45),
    ("com deploy",6,31),
    ("mariadb bootstrap",0,31),
    ("nova config",0,20),
    ("haproxy deploy",0,19),
    ("rabmq deploy",0,10),
    ("ovs deploy",0,10),
    ("nova pull",0,8),
    ("neutron pull",0,7),
    ("com ktb-deploy",0,6),
    ("glance pull",0,5),
    ("memc deploy",0,4),
    ("mariadb pull",0,4),
    ("kst pull",0,4)]


if __name__ == '__main__':
    gnuplot_file_from_list(tasks, "openstack4_novamdb_gantt.gnu", title='')
