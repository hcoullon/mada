# Content

- `real4_nova_mdb_trace.txt`: raw trace obtained from real experiments on three nodes of the Taurus cluster of [Grid'5000](https://www.grid5000.fr)
- `real4_nova_mdb_gantt.py`: python file to generate the gnuplot Gantt diagram
- `real4_nova_mdb_gantt.gnu`: gnuplot file of the Gantt diagram
- `real4_nova_mdb_gantt.pdf`: pdf file of the Gantt diagram