# Content

This directory contains one subdirectory per version of the OpenStack commissioning.

`0-deadlock` contains the raw trace of Roméo that detects the deadlock.

The four others directories contain:
- one directory `mada` for results returned by MADA
- one directory `real` for results obtained from real experiments on [Grid'5000](https://www.grid5000.fr)

Finally, this directory contains the file `gnuplot_gantt.py` that is used to generate the Gantt diagrams in subdirectories.