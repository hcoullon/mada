romeo-cli -v openstack1_naive.cts 

* Deployability

[info] Checking A (true U ((((( nova_deployed > 0 ) and (
keystone_deployed > 0 )) and ( neutron_deployed > 0 )) and (
glance_deployed > 0 )) and ( mariadb_deployed > 0 )))

[info] Computation mode is time

[info] Computation method is state classes.

[info] Convergence method is merge on common markings.

true

No trace.

[info] Time: 41.6s (total) = 39.5s (user) + 2.1s (system)

[info] Max memory used: 249.5Mo

* Parallelism
** nova

[info] Checking when true maximize ( ( ( nova_pull + nova_config ) + nova_register ) + nova_deploy )

[info] Computation mode is time

[info] Computation method is state classes.

[info] Convergence method is merge on common markings.

1

Trace: neutron_initiated_odocks, neutron_initiated_od0_neutron_pull,
openvswitch_initiated_odocks,
openvswitch_initiated_od0_openvswitch_deploy, nova_initiated_odocks,
nova_initiated_od0_nova_pull

[info] Time: 42.1s (total) = 39.7s (user) + 2.4s (system)

[info] Max memory used: 742.0Mo

** full

[info] Checking when true maximize ( ( ( ( ( ( ( ( ( ( ( ( ( ( ( ( ( (
( ( ( nova_pull + nova_config ) + nova_register ) + nova_deploy ) +
common_ktb_deploy ) + common_deploy ) + haproxy_deploy ) +
memcached_deploy ) + rabbitmq_deploy ) + mariadb_pull ) +
mariadb_deploy ) + keystone_pull ) + keystone_deploy ) + glance_pull
) + glance_config ) + glance_register ) + glance_deploy ) +
openvswitch_deploy ) + neutron_pull ) + neutron_config ) +
neutron_register ) + neutron_deploy )

[info] Computation mode is time

[info] Computation method is state classes.

[info] Convergence method is merge on common markings.

10

Trace: neutron_initiated_odocks, neutron_initiated_od0_neutron_pull,
openvswitch_initiated_odocks,
openvswitch_initiated_od0_openvswitch_deploy, nova_initiated_odocks,
nova_initiated_od0_nova_pull, glance_initiated_odocks,
glance_initiated_od0_glance_pull, keystone_initiated_odocks,
keystone_initiated_od0_keystone_pull, mariadb_initiated_odocks,
mariadb_initiated_od0_mariadb_pull, rabbitmq_initiated_odocks,
rabbitmq_initiated_od0_rabbitmq_deploy, memcached_initiated_odocks,
memcached_initiated_od0_memcached_deploy, haproxy_initiated_odocks,
haproxy_initiated_od0_haproxy_deploy, common_initiated_odocks,
common_initiated_od0_common_ktb_deploy

[info] Time: 43.2s (total) = 40.8s (user) + 2.4s (system)

[info] Max memory used: 742.7Mo

* Boundaries
** min = 575

[info] Checking mincost E (true U ((((( nova_deployed > 0 ) and (
keystone_deployed > 0 )) and ( neutron_deployed > 0 )) and (
glance_deployed > 0 )) and ( mariadb_deployed > 0 )))

[info] Computation mode is time

[info] Infimum cost computation uses simple cost state classes.

[info] Convergence method is merge on common markings.

575

Trace: neutron_initiated_odocks@0,
neutron_initiated_od0_neutron_pull@0, openvswitch_initiated_odocks@0,
openvswitch_initiated_od0_openvswitch_deploy@0,
nova_initiated_odocks@0, nova_initiated_od0_nova_pull@0,
glance_initiated_odocks@0, glance_initiated_od0_glance_pull@0,
keystone_initiated_odocks@0, keystone_initiated_od0_keystone_pull@0,
mariadb_initiated_odocks@0, mariadb_initiated_od0_mariadb_pull@0,
rabbitmq_initiated_odocks@0, rabbitmq_initiated_od0_rabbitmq_deploy@0,
memcached_initiated_odocks@0,
memcached_initiated_od0_memcached_deploy@0,
haproxy_initiated_odocks@0, haproxy_initiated_od0_haproxy_deploy@0,
common_initiated_odocks@0, common_initiated_od0_common_ktb_deploy@0,
keystone_pulled_id0_keystone_pull@4, keystone_pulled_idocks@4,
keystone_pulled_odocks@4, mariadb_pulled_id0_mariadb_pull@4,
mariadb_pulled_idocks@4, mariadb_pulled_odocks@4,
memcached_deployed_id0_memcached_deploy@4,
memcached_deployed_idocks@4, glance_pulled_id0_glance_pull@5,
common_ktb_deployed_id0_common_ktb_deploy@6,
common_ktb_deployed_idocks@6, common_ktb_deployed_odocks@6,
common_ktb_deployed_od0_common_deploy@6,
neutron_pulled_id0_neutron_pull@7, nova_pulled_id0_nova_pull@8,
rabbitmq_deployed_id0_rabbitmq_deploy@10,
openvswitch_deployed_id0_openvswitch_deploy@10,
openvswitch_deployed_idocks@10, rabbitmq_deployed_idocks@10,
haproxy_deployed_id0_haproxy_deploy@19, haproxy_deployed_idocks@19,
mariadb_pulled_od0_mariadb_deploy@19,
common_deployed_id0_common_deploy@31, common_deployed_idocks@31,
mariadb_deployed_id0_mariadb_deploy@81, mariadb_deployed_idocks@81,
neutron_initiated_od1_neutron_config@81,
nova_initiated_od1_nova_config@81,
glance_initiated_od1_glance_config@81,
keystone_pulled_od0_keystone_deploy@81,
glance_pulled_id1_glance_config@89, nova_pulled_id1_nova_config@101,
neutron_pulled_id1_neutron_config@107,
keystone_deployed_id0_keystone_deploy@179,
keystone_deployed_idocks@179,
neutron_initiated_od2_neutron_register@179,
nova_initiated_od2_nova_register@179,
glance_initiated_od2_glance_register@179,
neutron_pulled_id2_neutron_register@200, neutron_pulled_idocks@200,
neutron_pulled_odocks@200, neutron_pulled_od0_neutron_deploy@200,
glance_pulled_id2_glance_register@203, glance_pulled_idocks@203,
glance_pulled_odocks@203, glance_pulled_od0_glance_deploy@203,
nova_pulled_id2_nova_register@230, nova_pulled_idocks@230,
nova_pulled_odocks@230, nova_pulled_od0_nova_deploy@230,
glance_deployed_id0_glance_deploy@294, glance_deployed_idocks@294,
neutron_deployed_id0_neutron_deploy@400, neutron_deployed_idocks@400,
nova_deployed_id0_nova_deploy@575, nova_deployed_idocks@575

[info] Time: 130.1s (total) = 128.7s (user) + 1.3s (system)

[info] Max memory used: 1983.5Mo

** max = 615

[info] Checking mincost E (true U ((((( nova_deployed > 0 ) and (
keystone_deployed > 0 )) and ( neutron_deployed > 0 )) and (
glance_deployed > 0 )) and ( mariadb_deployed > 0 )))

[info] Computation mode is time

[info] Infimum cost computation uses simple cost state classes.

[info] Convergence method is merge on common markings.

-615

Trace: neutron_initiated_odocks@0,
neutron_initiated_od0_neutron_pull@0, openvswitch_initiated_odocks@0,
openvswitch_initiated_od0_openvswitch_deploy@0,
nova_initiated_odocks@0, nova_initiated_od0_nova_pull@0,
glance_initiated_odocks@0, glance_initiated_od0_glance_pull@0,
keystone_initiated_odocks@0, keystone_initiated_od0_keystone_pull@0,
mariadb_initiated_odocks@0, mariadb_initiated_od0_mariadb_pull@0,
rabbitmq_initiated_odocks@0, rabbitmq_initiated_od0_rabbitmq_deploy@0,
memcached_initiated_odocks@0,
memcached_initiated_od0_memcached_deploy@0,
haproxy_initiated_odocks@0, haproxy_initiated_od0_haproxy_deploy@0,
common_initiated_odocks@0, common_initiated_od0_common_ktb_deploy@0,
glance_pulled_id0_glance_pull@5, keystone_pulled_id0_keystone_pull@5,
keystone_pulled_idocks@5, keystone_pulled_odocks@5,
mariadb_pulled_id0_mariadb_pull@5, mariadb_pulled_idocks@5,
mariadb_pulled_odocks@5, memcached_deployed_id0_memcached_deploy@5,
memcached_deployed_idocks@5, neutron_pulled_id0_neutron_pull@7,
common_ktb_deployed_id0_common_ktb_deploy@7,
common_ktb_deployed_idocks@7, common_ktb_deployed_odocks@7,
common_ktb_deployed_od0_common_deploy@7, nova_pulled_id0_nova_pull@8,
rabbitmq_deployed_id0_rabbitmq_deploy@9, rabbitmq_deployed_idocks@9,
openvswitch_deployed_id0_openvswitch_deploy@10,
openvswitch_deployed_idocks@10,
haproxy_deployed_id0_haproxy_deploy@20, haproxy_deployed_idocks@20,
mariadb_pulled_od0_mariadb_deploy@20,
common_deployed_id0_common_deploy@32, common_deployed_idocks@32,
mariadb_deployed_id0_mariadb_deploy@87, mariadb_deployed_idocks@87,
neutron_initiated_od1_neutron_config@87,
nova_initiated_od1_nova_config@87,
glance_initiated_od1_glance_config@87,
keystone_pulled_od0_keystone_deploy@87,
glance_pulled_id1_glance_config@95, nova_pulled_id1_nova_config@107,
neutron_pulled_id1_neutron_config@113,
keystone_deployed_id0_keystone_deploy@193,
keystone_deployed_idocks@193,
neutron_initiated_od2_neutron_register@193,
nova_initiated_od2_nova_register@193,
glance_initiated_od2_glance_register@193,
neutron_pulled_id2_neutron_register@214, neutron_pulled_idocks@214,
neutron_pulled_odocks@214, neutron_pulled_od0_neutron_deploy@214,
glance_pulled_id2_glance_register@217, glance_pulled_idocks@217,
glance_pulled_odocks@217, glance_pulled_od0_glance_deploy@217,
nova_pulled_id2_nova_register@251, nova_pulled_idocks@251,
nova_pulled_odocks@251, nova_pulled_od0_nova_deploy@251,
glance_deployed_id0_glance_deploy@308, glance_deployed_idocks@308,
neutron_deployed_id0_neutron_deploy@414, neutron_deployed_idocks@414,
nova_deployed_id0_nova_deploy@615, nova_deployed_idocks@615

[info] Time: 128.8s (total) = 127.6s (user) + 1.1s (system)

[info] Max memory used: 1982.8Mo



