import sys
sys.path.insert(0, '../../')
from gnuplot_gantt import *

tasks = [
    ("glance deploy",203,294),
    ("neutron deploy", 200,400),
    ("nova deploy",230,575),
    ("nova register", 179,230),
    ("glance register", 179,203),
    ("neutron register", 179,200),
    ("kst deploy", 81,179),
    ("nova config", 81,101),
    ("neutron config",81,107),
    ("glance config", 81, 89),
    ("mariadb deploy",19,81),
    ("com deploy",6,31),
    ("haproxy deploy",0,19),
    ("rabmq deploy",0,10),
    ("ovs deploy",0,10),
    ("nova pull",0,8),
    ("neutron pull",0,7),
    ("com ktb-deploy",0,6),
    ("glance pull", 0, 5),
    ("memc deploy", 0, 4),
    ("mariadb pull",0,4),
    ("kst pull",0,4)]

if __name__ == '__main__':
    gnuplot_file_from_list(tasks, "openstack1_naive_gantt.gnu", title='')
