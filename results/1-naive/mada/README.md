# Content

- `openstack1_naive_results.org`: traces obtained from Roméo for each experiment
- `openstack1_naive_gantt.py`: python file to generate the Gantt diagram in gnuplot
- `openstack1_naive_gantt.gnu`: gnuplot file of the Gantt diagram
- `openstack1_naive_gantt.pdf`: pdf file of the Gantt diagram