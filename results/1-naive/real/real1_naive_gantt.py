import sys
sys.path.insert(0, '../../')
from gnuplot_gantt import *

tasks = [
    ("glance deploy",188,265),
    ("neutron deploy", 184,396),
    ("nova deploy",212,495),
    ("nova register", 161,211),
    ("glance register", 161,185),
    ("neutron register", 161,181),
    ("kst deploy", 94,161),
    ("nova config", 94,127),
    ("neutron config",94,119),
    ("glance config", 94, 102),
    ("mariadb deploy",19,94),
    ("com deploy",6,31),
    ("haproxy deploy",0,19),
    ("rabmq deploy",0,10),
    ("ovs deploy",0,10),
    ("nova pull",0,15),
    ("neutron pull",0,9),
    ("com ktb-deploy",0,6),
    ("glance pull", 0, 5),
    ("memc deploy", 0, 5),
    ("mariadb pull",0,4),
    ("kst pull",0,4)]

if __name__ == '__main__':
    gnuplot_file_from_list(tasks, "real1_naive_gantt.gnu", title='')
