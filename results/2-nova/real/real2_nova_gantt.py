import sys
sys.path.insert(0, '../../')
from gnuplot_gantt import *

tasks = [
    ("glance deploy",189,283),
    ("neutron deploy",189,402),
    ("nova deploy",371,492),
    ("nova upg-db",217,371),
    ("nova upg-api-db",217,270),
    ("nova register",161,217),
    ("glance register",161,189),
    ("neutron register",161,189),
    ("kst deploy",96,161),
    ("nova create_db",96,111),
    ("glance config",96,104),
    ("neutron config",96,122),
    ("mariadb deploy",20,96),
    ("com deploy",6,30),
    ("nova config",0,22),
    ("haproxy deploy",0,19),
    ("rabmq deploy",0,10),
    ("ovs deploy",0,11),
    ("nova pull",0,15),
    ("neutron pull",0,10),
    ("com ktb-deploy",0,7),
    ("glance pull",0,6),
    ("memc deploy",0,5),
    ("mariadb pull",0,4),
    ("kst pull",0,4)
]
    

if __name__ == '__main__':
    gnuplot_file_from_list(tasks, "real2_nova_gantt.gnu", title='')
