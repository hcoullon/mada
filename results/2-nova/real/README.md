# Content

- `real2_nova_trace.txt`: raw trace obtained from real experiments on three nodes of the Taurus cluster of [Grid'5000](https://www.grid5000.fr)
- `real2_nova_gantt.py`: python file to generate the gnuplot Gantt diagram
- `real2_nova_gantt.gnu`: gnuplot file of the Gantt diagram
- `real2_nova_gantt.pdf`: pdf file of the Gantt diagram