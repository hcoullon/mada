# Content

- `openstack2_nova_results.org`: traces obtained from Roméo for each experiment
- `openstack2_nova_gantt.py`: python file to generate the Gantt diagram in gnuplot
- `openstack2_nova_gantt.gnu`: gnuplot file of the Gantt diagram
- `openstack2_nova_gantt.pdf`: pdf file of the Gantt diagram