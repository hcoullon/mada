set xrange [0.000000:397.000000]
set yrange [0.400000:25.600000]
set autoscale x
set xlabel "Data Range"
set ylabel ""
set title ""
set ytics ("kst pull" 1, "mariadb pull" 2, "memc deploy" 3, "glance pull" 4, "com ktb-deploy" 5, "neutron pull" 6, "nova pull" 7, "ovs deploy" 8, "rabmq deploy" 9, "haproxy deploy" 10, "nova config" 11, "com deploy" 12, "mariadb deploy" 13, "glance config" 14, "neutron config" 15, "nova create-db" 16, "kst deploy" 17, "nova upg-api-db" 18, "nova upgrade-db" 19, "neutron register" 20, "glance register" 21, "nova register" 22, "nova deploy" 23, "neutron deploy" 24, "glance deploy" 25)
unset key
set grid xtics
set palette model RGB defined ( 0 1.0 0.8 0.8, 1 1.0 0.8 1.0, 2 0.8 0.8 1.0, 3 0.8 1.0 1.0, 4 0.8 1.0 0.8, 5 1.0 1.0 0.8 )
unset colorbox
set object 1 rectangle from 212.000000, 24.6 to 306.000000, 25.4 fillcolor palette frac 1.00 fillstyle solid 0.8
set object 2 rectangle from 211.000000, 23.6 to 397.000000, 24.4 fillcolor palette frac 0.96 fillstyle solid 0.8
set object 3 rectangle from 206.000000, 22.6 to 395.000000, 23.4 fillcolor palette frac 0.92 fillstyle solid 0.8
set object 4 rectangle from 187.000000, 21.6 to 237.000000, 22.4 fillcolor palette frac 0.88 fillstyle solid 0.8
set object 5 rectangle from 187.000000, 20.6 to 212.000000, 21.4 fillcolor palette frac 0.83 fillstyle solid 0.8
set object 6 rectangle from 187.000000, 19.6 to 211.000000, 20.4 fillcolor palette frac 0.79 fillstyle solid 0.8
set object 7 rectangle from 106.000000, 18.6 to 206.000000, 19.4 fillcolor palette frac 0.75 fillstyle solid 0.8
set object 8 rectangle from 106.000000, 17.6 to 151.000000, 18.4 fillcolor palette frac 0.71 fillstyle solid 0.8
set object 9 rectangle from 92.000000, 16.6 to 187.000000, 17.4 fillcolor palette frac 0.67 fillstyle solid 0.8
set object 10 rectangle from 92.000000, 15.6 to 106.000000, 16.4 fillcolor palette frac 0.62 fillstyle solid 0.8
set object 11 rectangle from 92.000000, 14.6 to 118.000000, 15.4 fillcolor palette frac 0.58 fillstyle solid 0.8
set object 12 rectangle from 92.000000, 13.6 to 100.000000, 14.4 fillcolor palette frac 0.54 fillstyle solid 0.8
set object 13 rectangle from 16.000000, 12.6 to 92.000000, 13.4 fillcolor palette frac 0.50 fillstyle solid 0.8
set object 14 rectangle from 7.000000, 11.6 to 33.000000, 12.4 fillcolor palette frac 0.46 fillstyle solid 0.8
set object 15 rectangle from 0.000000, 10.6 to 23.000000, 11.4 fillcolor palette frac 0.42 fillstyle solid 0.8
set object 16 rectangle from 0.000000, 9.6 to 16.000000, 10.4 fillcolor palette frac 0.38 fillstyle solid 0.8
set object 17 rectangle from 0.000000, 8.6 to 10.000000, 9.4 fillcolor palette frac 0.33 fillstyle solid 0.8
set object 18 rectangle from 0.000000, 7.6 to 11.000000, 8.4 fillcolor palette frac 0.29 fillstyle solid 0.8
set object 19 rectangle from 0.000000, 6.6 to 14.000000, 7.4 fillcolor palette frac 0.25 fillstyle solid 0.8
set object 20 rectangle from 0.000000, 5.6 to 10.000000, 6.4 fillcolor palette frac 0.21 fillstyle solid 0.8
set object 21 rectangle from 0.000000, 4.6 to 7.000000, 5.4 fillcolor palette frac 0.17 fillstyle solid 0.8
set object 22 rectangle from 0.000000, 3.6 to 6.000000, 4.4 fillcolor palette frac 0.12 fillstyle solid 0.8
set object 23 rectangle from 0.000000, 2.6 to 5.000000, 3.4 fillcolor palette frac 0.08 fillstyle solid 0.8
set object 24 rectangle from 0.000000, 1.6 to 4.000000, 2.4 fillcolor palette frac 0.04 fillstyle solid 0.8
set object 25 rectangle from 0.000000, 0.6 to 4.000000, 1.4 fillcolor palette frac 0.00 fillstyle solid 0.8
plot -1 with lines linecolor palette frac 0.00  linewidth 6, \
	-1 with lines linecolor palette frac 0.04  linewidth 6, \
	-1 with lines linecolor palette frac 0.08  linewidth 6, \
	-1 with lines linecolor palette frac 0.12  linewidth 6, \
	-1 with lines linecolor palette frac 0.17  linewidth 6, \
	-1 with lines linecolor palette frac 0.21  linewidth 6, \
	-1 with lines linecolor palette frac 0.25  linewidth 6, \
	-1 with lines linecolor palette frac 0.29  linewidth 6, \
	-1 with lines linecolor palette frac 0.33  linewidth 6, \
	-1 with lines linecolor palette frac 0.38  linewidth 6, \
	-1 with lines linecolor palette frac 0.42  linewidth 6, \
	-1 with lines linecolor palette frac 0.46  linewidth 6, \
	-1 with lines linecolor palette frac 0.50  linewidth 6, \
	-1 with lines linecolor palette frac 0.54  linewidth 6, \
	-1 with lines linecolor palette frac 0.58  linewidth 6, \
	-1 with lines linecolor palette frac 0.62  linewidth 6, \
	-1 with lines linecolor palette frac 0.67  linewidth 6, \
	-1 with lines linecolor palette frac 0.71  linewidth 6, \
	-1 with lines linecolor palette frac 0.75  linewidth 6, \
	-1 with lines linecolor palette frac 0.79  linewidth 6, \
	-1 with lines linecolor palette frac 0.83  linewidth 6, \
	-1 with lines linecolor palette frac 0.88  linewidth 6, \
	-1 with lines linecolor palette frac 0.92  linewidth 6, \
	-1 with lines linecolor palette frac 0.96  linewidth 6, \
	-1 with lines linecolor palette frac 1.00  linewidth 6